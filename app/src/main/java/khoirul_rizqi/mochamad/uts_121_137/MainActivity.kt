package khoirul_rizqi.mochamad.uts_121_137

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragLogin : FragmentLogin
    lateinit var fragMhs : FragmentMhs
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragLogin = FragmentLogin()
        fragMhs = FragmentMhs()
        db =DBOpenHelper(this).writableDatabase
    }

    fun getDBObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
       when(p0.itemId){
           R.id.itemLogin ->{
               ft = supportFragmentManager.beginTransaction()
               ft.replace(R.id.frameLayout,fragLogin).commit()
               frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
               frameLayout.visibility = View.VISIBLE
           }
           R.id.itemMahasiswa ->{
               ft = supportFragmentManager.beginTransaction()
               ft.replace(R.id.frameLayout,fragMhs).commit()
               frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
               frameLayout.visibility = View.VISIBLE
           }
           R.id.itemAbout -> frameLayout.visibility = View.GONE
       }
        return true
    }
}
