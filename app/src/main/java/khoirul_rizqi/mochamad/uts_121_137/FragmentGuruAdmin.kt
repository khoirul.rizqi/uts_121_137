package khoirul_rizqi.mochamad.uts_121_137

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_guru.view.*

class FragmentGuruAdmin : Fragment(), View.OnClickListener {
    lateinit var thisParent : AdminActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var dialog : AlertDialog.Builder
    var iD : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as AdminActivity
        db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_data_guru,container,false)
        dialog = AlertDialog.Builder(thisParent)
        v.btnUpdate.setOnClickListener(this)
        v.lvGuru.setOnItemClickListener(dimana)
        return v
    }

    fun showDataGuru(){
        var sql=""
        sql="select id_user as _id, usern, passw, level from user where level='guru'"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(thisParent, R.layout.item_data_guru,c, arrayOf("_id", "usern", "passw","level"), intArrayOf(R.id.iUser, R.id.Username, R.id.Password, R.id.lvl), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lvGuru.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataGuru()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnUpdate->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apakah anda ingin mengubahnya ?")
                    .setPositiveButton("ya",btnUpdateDialog)
                    .setNegativeButton("tidak",null)
                dialog.show()
            }
        }
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataGuru(v.txUser.text.toString(),v.txPass.text.toString(),iD)
        v.txUser.setText("")
        v.txPass.setText("")
    }

    fun updateDataGuru(a:String,b:String,I:String){
        var cv : ContentValues = ContentValues()
        cv.put("usern",a)
        db.update("user",cv,"id_user=$I",null)
        cv.put("passw",b)
        db.update("user",cv,"id_user=$I",null)
        v.txUser.setText("")
        v.txPass.setText("")
        showDataGuru()
    }

    val dimana = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        iD = c.getString(c.getColumnIndex("_id"))
        v.txUser.setText(c.getString(c.getColumnIndex("usern")))
        v.txPass.setText(c.getString(c.getColumnIndex("passw")))
    }
}