package khoirul_rizqi.mochamad.uts_121_137

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_mahasiswa.view.*

class FragmentMhsGuru : Fragment() {

    lateinit var thisParent : GuruActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as GuruActivity
        db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_data_mahasiswa,container,false)
        return v
    }

    fun showDataMhs(){
        val cursor : Cursor = db.query("mhs", arrayOf("nim as _id","nama"),
            null,null,null,null,"nama")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_mhs,cursor,
            arrayOf("_id","nama"), intArrayOf(R.id.txIdMhs,R.id.txNamaMhs), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMhs.adapter = adapter
    }

    override fun onStart(){
        super.onStart()
        showDataMhs()
    }
}