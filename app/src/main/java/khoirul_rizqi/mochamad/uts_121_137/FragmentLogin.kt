package khoirul_rizqi.mochamad.uts_121_137

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_login.*
import kotlinx.android.synthetic.main.frag_data_login.view.*

class FragmentLogin : Fragment(), View.OnClickListener{
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("apakah anda ingin login ?")
                    .setPositiveButton("ya",btnLoginDialog)
                    .setNegativeButton("tidak",null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var dialog : AlertDialog.Builder
    var a : String = ""
    var b : String = ""
    var level : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_data_login,container,false)
        dialog = AlertDialog.Builder(thisParent)
        v.btnLogin.setOnClickListener(this)
        return v
    }

    fun hapus(){
        edUser.setText("")
        edPass.setText("")
    }

    val btnLoginDialog = DialogInterface.OnClickListener { dialog, which ->
        a = edUser.text.toString()
        b = edPass.text.toString()

        if(cekAdmin(a,b)){
            val Admin = Intent(activity,AdminActivity::class.java)
            startActivity(Admin)
        }
        else if(cekGuru(a,b)){
            val Guru = Intent(activity,GuruActivity::class.java)
            startActivity(Guru)
        }
        hasil.setText("GAGAL LOGIN")
    }

    fun cekAdmin( u : String,p : String):Boolean{
        var sql = "select * from user where usern='$u' and passw='$p' and level='admin'"
        val hasil : Cursor = db.rawQuery(sql,null)
        if(hasil.count>0){
            return true
        }
        return false
    }

    fun cekGuru(u:String,p:String):Boolean{
        var sql = "select * from user where usern='$u' and passw='$p' and level='guru'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            return true
        }
        return false
    }
}