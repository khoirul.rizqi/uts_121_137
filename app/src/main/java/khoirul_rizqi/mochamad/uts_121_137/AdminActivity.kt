package khoirul_rizqi.mochamad.uts_121_137

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener{

    val activity = this@AdminActivity
    lateinit var fragMhsAdmin : FragmentMhsAdmin
    lateinit var fragGuruAdmin : FragmentGuruAdmin
    lateinit var ft : FragmentTransaction
    lateinit var db : SQLiteDatabase

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemAbsensiAdmin ->frameLayoutAdmin.visibility = View.GONE
            R.id.itemMhsAdmin ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayoutAdmin,fragMhsAdmin).commit()
                frameLayoutAdmin.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayoutAdmin.visibility = View.VISIBLE
            }
            R.id.itemDosenAdmin ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayoutAdmin,fragGuruAdmin).commit()
                frameLayoutAdmin.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayoutAdmin.visibility = View.VISIBLE
            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        BNVAdmin.setOnNavigationItemSelectedListener(this)
        fragMhsAdmin = FragmentMhsAdmin()
        fragGuruAdmin = FragmentGuruAdmin()
        db = DBOpenHelper(this).readableDatabase
    }

    fun getDBObject() : SQLiteDatabase{
        return db
    }
}