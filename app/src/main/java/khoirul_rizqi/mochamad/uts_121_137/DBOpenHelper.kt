package khoirul_rizqi.mochamad.uts_121_137

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tuser = "create table user(id_user integer primary key autoincrement, usern text not null, passw text not null,level text not null)"
        val insuser = "insert into user(usern,passw,level) values ('erlang','12345678','admin'),('khoi','12345678','admin'),('Heru','12345678','guru'),('Ika','12345678','guru')"
        val tMhs = "create table mhs(id_mhs integer primary key autoincrement,nim text not null, nama text not null)"
        val insMhs = "insert into mhs(nim,nama) values('1931733121','Erlang Dhika Wilis Mahendra'),('1931733137','Mochammad Khoirul Rizqi'),('1931733134','Mochammad Rizal Priyandana'),('1931733144','Ichbal Yuda Backtiar'),('1931733131','Afifah Wulandari')"
        db?.execSQL(tMhs)
        db?.execSQL(insMhs)
        db?.execSQL(tuser)
        db?.execSQL(insuser)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object{
        val DB_Name = "mahasiswa"
        val DB_Ver = 1
    }
}