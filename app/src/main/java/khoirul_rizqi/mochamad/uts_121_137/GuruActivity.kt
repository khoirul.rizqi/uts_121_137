package khoirul_rizqi.mochamad.uts_121_137

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_guru.*

class GuruActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    val activity = this@GuruActivity
    lateinit var fragMhsGuru : FragmentMhsGuru
    lateinit var ft : FragmentTransaction
    lateinit var db : SQLiteDatabase

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemMhsGuru ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayoutGuru,fragMhsGuru).commit()
                frameLayoutGuru.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayoutGuru.visibility = View.VISIBLE
            }
            R.id.itemAboutGuru ->frameLayoutGuru.visibility = View.GONE
            R.id.itemAbsenGuru ->{

            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guru)
        bottomNavigationView2.setOnNavigationItemSelectedListener(this)
        fragMhsGuru = FragmentMhsGuru()
        db = DBOpenHelper(this).readableDatabase

    }

    fun getDBObject() : SQLiteDatabase{
        return db
    }
}